(ns logic.core
  (:use [clojure.core.logic]) 
  (:require [clojure.core.logic.fd :as fd])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(run* [ssend mmore mmoney]
  (fresh [s e n d m o r y snd mre mny]
    (fd/in s (fd/interval 1 9)) 
    (fd/in e (fd/interval 0 9)) 
    (fd/in n (fd/interval 0 9)) 
    (fd/in d (fd/interval 0 9)) 
    (fd/in m (fd/interval 1 9)) 
    (fd/in o (fd/interval 0 9)) 
    (fd/in r (fd/interval 0 9)) 
    (fd/in y (fd/interval 0 9))
    (fd/in snd (fd/interval 0 9999))
    (fd/in mre (fd/interval 0 9999))
    (fd/in mny (fd/interval 0 99999))
    (fd/eq (= snd (+ (* s 1000) (* e 100) (* n 10) d)))
    (fd/eq (= mre (+ (* m 1000) (* o 100) (* r 10) e)))
    (fd/eq (= mny (+ (* m 10000) (* o 1000) (* n 100) (* e 10) y)))
    (fd/eq (= mny (+ snd mre)))
    (distincto [s e n d m o r y])
    (== mmoney mny)
    (== ssend snd)
    (== mmore mre)))
      
